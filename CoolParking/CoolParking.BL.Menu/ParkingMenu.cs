﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CoolParking.BL.Menu
{
    class ParkingMenu
    {
        private readonly ParkingService parking;

        public ParkingMenu()
        {
            TimerService withdrawTimer = new TimerService();
            TimerService logTimer = new TimerService();
            LogService logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

            parking = new ParkingService(withdrawTimer, logTimer, logService);

            withdrawTimer.Start();
            logTimer.Start();
        }

        public void Run()
        {
            ShowOptions();
        }
        
        private void ShowOptions()
        {
            Console.WriteLine("Press the number to select the desired action");
            
            Console.WriteLine("1 -  Display parking balance");
            
            Console.WriteLine("2 -  Display income of the current session");
            
            Console.WriteLine("3 -  Display free places in the parking");
            
            Console.WriteLine("4 -  Display current transactions");
            
            Console.WriteLine("5 -  Display the history of transactions");
            
            Console.WriteLine("6 -  Display all the vehicles in the parking");
            
            Console.WriteLine("7 -  Add new vehicle to the parking");
            
            Console.WriteLine("8 -  Remove vehicle from the parking");
            
            Console.WriteLine("9 -  Top up the balance of selected vehicle");

            ChooseOption(AcceptOption());
        }

        private void ChooseOption(int option)
        {
            switch (option)
            {
                case 1:
                    DisplayParkingBalance();
                    ReturnToMenu();
                    break;

                case 2:
                    DisplayIncome();
                    ReturnToMenu();
                    break;

                case 3:
                    DisplayFreePlaces();
                    ReturnToMenu();
                    break;

                case 4:
                    DisplayCurrentTransactions();
                    ReturnToMenu();
                    break;

                case 5:
                    DisplayHistory();
                    ReturnToMenu();
                    break;

                case 6:
                    DisplayAllVehicles();
                    ReturnToMenu();
                    break;

                case 7:
                    AddNewVehicle();
                    ReturnToMenu();
                    break;

                case 8:
                    RemoveVehicleFromPaking();
                    ReturnToMenu();
                    break;

                case 9:
                    TopUpVehicle();
                    ReturnToMenu();
                    break;

                default:
                    Console.WriteLine("Wrong button. Please press numbers from 1 to 9 on the keybord");
                    ShowOptions();
                    break;
            }
        }

        private int AcceptOption()
        {
            Console.WriteLine("");

            int option = -1;

            try
            {
                option = int.Parse(Console.ReadLine());
            }
            
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return option;
        }
        
        private void ReturnToMenu()
        {
            Console.WriteLine("Press any button to go back");
            Console.ReadKey();
            Console.Clear();
            ShowOptions();
        }

        private void DisplayParkingBalance()
        {
            Console.WriteLine($"The balance of the parking is: {parking.GetBalance()}");
        }

        private void DisplayIncome()
        {
            Console.WriteLine($"Income of the current session is: {parking.GetLastParkingTransactions().Sum(v => v.Sum)}");
        }

        private void DisplayFreePlaces()
        {
            Console.WriteLine($"Currently there are {parking.GetFreePlaces()} free places out of {parking.GetCapacity()}");
        }

        private void DisplayCurrentTransactions()
        {
            foreach(TransactionInfo trans in parking.GetLastParkingTransactions())
            {
                Console.WriteLine(trans.ToString() + "\n");
            }
        }

        private void DisplayHistory()
        {
            try
            {
                Console.WriteLine(parking.ReadFromLog());
            }
            
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void DisplayAllVehicles()
        {
            foreach (Vehicle car in parking.GetVehicles())
            {
                Console.WriteLine(car.ToString() + "\n");
            }
        }

        private void AddNewVehicle()
        {
            string id = Vehicle.GenerateRandomRegistrationPlateNumber();

            try
            {
                Console.WriteLine("Specify the vehicle type (Motorcycle, PassengerCar, Bus, Truck): ");
                var type = (VehicleType)Enum.Parse(typeof(VehicleType), Console.ReadLine());

                Console.WriteLine("Specify the balance: (type out the amount)");
                var balance = int.Parse(Console.ReadLine());

                parking.AddVehicle(new Vehicle(id, type, balance));

                Console.WriteLine($"The id of your vehicle is {id}");
            }

            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        private void RemoveVehicleFromPaking()
        {
            Console.WriteLine("Please provide the Id: ");
            string id = Console.ReadLine();

            try
            {
                parking.RemoveVehicle(id);
            }

            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void TopUpVehicle()
        {
            Console.WriteLine("Please provide the Id: ");
            string id = Console.ReadLine();

            try
            {
                Console.WriteLine("Please specify the amount of money you want to add: ");
                int sum = int.Parse(Console.ReadLine());

                parking.TopUpVehicle(id, sum);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}

﻿using CoolParking.BL.Menu;
using System;

namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {
            new ParkingMenu().Run();
        }
    }
}

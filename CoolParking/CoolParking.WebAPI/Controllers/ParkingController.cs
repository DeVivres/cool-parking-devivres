﻿    using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parking;

        public ParkingController(IParkingService parking)
        {
            _parking = parking; 
        }

        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parking.GetBalance());
        }

        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parking.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parking.GetFreePlaces());
        }
    }
}
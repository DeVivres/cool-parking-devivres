﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : Controller
    {
        private readonly IParkingService _parking;

        public TransactionsController(IParkingService parking)
        {
            _parking = parking;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransaction()
        {
            var lastTransaction = _parking.GetLastParkingTransactions();

            return Ok(lastTransaction);
        }

        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            string Transactions;

            try
            {
                Transactions = _parking.ReadFromLog();
            }

            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(Transactions);
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle(ExtraTopUpVehicle extraTopUpVehicle)
        {
            string example = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
            if (!Regex.IsMatch(extraTopUpVehicle.Id, example))
            {
                return StatusCode(400, "Id is invalid, please provide the right one");
            }
            
            if (extraTopUpVehicle.Sum < 0)
            {
                return StatusCode(400, "The sum must be positive");
            }

            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == extraTopUpVehicle.Id);

            if (vehicle == null)
            {
                return StatusCode(404, "Vehicle is not found");
            }
            
            _parking.TopUpVehicle(extraTopUpVehicle.Id, extraTopUpVehicle.Sum);

            return Ok(vehicle);

        }
    }
}
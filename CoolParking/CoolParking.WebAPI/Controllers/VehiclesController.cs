﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : Controller
    {
        private readonly IParkingService _parking;
              
        public VehiclesController(IParkingService parking)
        {
            _parking = parking;
        }

        // +
        [HttpGet]
        public IActionResult GetVehicles()
        {
            var vehicles = _parking.GetVehicles().ToList();

            return Ok(vehicles);
        }

        // +
        [HttpGet("{id}")]
        public IActionResult GetVehicleById(string id)
        {
            string example = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
            if (!Regex.IsMatch(id, example))
            {
                return StatusCode(400, "Id is invalid, please provide the right one");
            }

            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return StatusCode(404, "Vehicle is not found");
            }

            return Ok(vehicle);
        }
        
        // +
        [HttpPost]
        public IActionResult AddVehicle([FromBody]System.Text.Json.JsonElement body)
        {
            string json = System.Text.Json.JsonSerializer.Serialize(body);

            if(! new ExtraVehicle().ExtraRequest(json))
            {
                try
                {
                    Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(json);
                    _parking.AddVehicle(vehicle);
                    return StatusCode(201, "Vehicle created");
                }

                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

            return BadRequest("Body is invalid");
        }

        // +
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            string example = @"[A-Z]{2}-\d{4}-[A-Z]{2}";
            if (!Regex.IsMatch(id, example))
            {
                return StatusCode(400, "Id is invalid, please provide the right one");
            }

            var vehicle = _parking.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
            {
                return StatusCode(404, "The vehicle is not found");
            }

            _parking.RemoveVehicle(id);

            return StatusCode(204, "The vehicle is deleted");
        }
    }
}
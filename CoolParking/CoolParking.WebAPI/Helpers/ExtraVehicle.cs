﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Helpers
{
    public class ExtraVehicle
    {
        public bool ExtraRequest(string json)
        {
            if (json.Split(',').Length != 3)
            {
                return true;
            }

            if (json.Contains("id") && json.Contains("vehicleType") && json.Contains("balance"))
            {
                return false;
            }

            return true;
        }
    }
}

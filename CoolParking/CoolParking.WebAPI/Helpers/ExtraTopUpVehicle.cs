﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Helpers
{
    public class ExtraTopUpVehicle
    {
        public ExtraTopUpVehicle() { }

        [JsonConstructor]
        public ExtraTopUpVehicle(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }
}

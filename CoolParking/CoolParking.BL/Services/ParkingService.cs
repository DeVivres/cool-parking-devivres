﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ILogService _logService;

        private readonly List<TransactionInfo> _transactions;

        private readonly Parking _parking;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _transactions = new List<TransactionInfo>();
            _parking = Parking.Instance;
            _logService = logService;

            withdrawTimer.Interval = Settings.PaymentTimer;
            withdrawTimer.Elapsed += Withdraw;

            logTimer.Interval = Settings.LoggingTimer;
            logTimer.Elapsed += Log;
        }

        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - Parking.Instance.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.Instance.Vehicles.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (ExistingId(vehicle.Id))
            {
                throw new ArgumentException("The vehicle with such Id already exists in the parking");
            }

            if (Settings.Capacity >= Parking.Instance.Vehicles.Count)
            {
                Parking.Instance.Vehicles.Add(vehicle);
            }

            else
            {
                throw new InvalidOperationException("The parking lot is full. Please come back later");
            }
        }

        private bool ExistingId(string id)
        {
            return Parking.Instance.Vehicles.Any(v => v.Id == id);
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle car = Parking.Instance.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (car == null)
            {
                throw new ArgumentException("The vehicle with such id does not exist");
            }

            if (car.Balance < 0)
            {
                throw new InvalidOperationException("Top up vehicle to take it out from the parking lot");
            }

            Parking.Instance.Vehicles.Remove(car);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle car = Parking.Instance.Vehicles.Where(v => v.Id == vehicleId).FirstOrDefault();

            if (car == null || sum < 0)
            {
                throw new ArgumentException("The vehicle does not exist or the sum is negative");
            }

            car.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {
            Parking.Instance.Balance = Settings.InitialBalance;
            Parking.Instance.Vehicles.Clear();
        }

        public void Log(object source, ElapsedEventArgs e)
        {
            var log = string.Empty;

            foreach (TransactionInfo transaction in _transactions)
            {
                log += transaction.ToString() + "\n";
            }

            _logService.Write(log);

            _transactions.Clear();
        }

        public void Withdraw(object source, ElapsedEventArgs e)
        {
            decimal total = 0;

            foreach (Vehicle car in _parking.Vehicles)
            {
                decimal sum = Settings.GetTariffs(car.VehicleType);

                if (car.Balance <= 0)
                {
                    sum *= Settings.Fine;
                }

                else if (car.Balance - sum < 0)
                {
                    decimal rest = sum - car.Balance;

                    sum = car.Balance + rest * Settings.Fine;
                }

                _transactions.Add(new TransactionInfo(DateTime.Now.ToString("h:mm:ss tt"), car.Id, sum));
                total += sum;
                car.Balance -= sum;
            }

            _parking.Balance += total;
        }
    }
}

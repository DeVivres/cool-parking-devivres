﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using CoolParking.BL.Extra;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        readonly public string Id;

        readonly public VehicleType VehicleType;

        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            string example = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (!Regex.IsMatch(id, example) || balance < 0)
            {
                throw new ArgumentException("The id is not valid or balance is negative");
            }

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            RandomIdentifier rand = new RandomIdentifier();
            string result = rand.randomIdentifier(8);
            return result;
        }

        public override string ToString()
        {
            return $"{Id}\n{VehicleType}\n{Balance}";
        }
    }
}
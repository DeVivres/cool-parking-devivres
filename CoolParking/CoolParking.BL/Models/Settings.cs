﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialBalance = 0;

        public const int Capacity = 10;

        public const int PaymentTimer = 5000;

        public const int LoggingTimer = 60000;

        public static decimal GetTariffs(VehicleType vehicleType)
        {
            decimal Tariff = 0m;

            switch (vehicleType)
            {
                case VehicleType.Motorcycle:
                    Tariff = 1m;
                    break;

                case VehicleType.PassengerCar:
                    Tariff = 2m;
                    break;

                case VehicleType.Bus:
                    Tariff = 3.5m;
                    break;

                case VehicleType.Truck:
                    Tariff = 5m;
                    break;
            }
            return Tariff;
        }
        public const decimal Fine = 2.5m;
    }
}
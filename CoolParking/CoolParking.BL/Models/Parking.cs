﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    sealed class Parking
    {
        private Parking() { }

        private static Parking instance = null;

        public static Parking Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Parking();
                }
                return instance;
            }
        }

        public decimal Balance = Settings.InitialBalance;

        public List<Vehicle> Vehicles = new List<Vehicle>();
    }
}
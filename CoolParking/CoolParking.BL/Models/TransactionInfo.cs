﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string Time { get; private set; }

        public string Id { get; private set; }

        public decimal Sum { get; private set; }

        public TransactionInfo(string time, string id, decimal sum)
        {
            Time = time;
            Id = id;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"{Time}\n{Id}\n{Sum}";
        }
    }
}

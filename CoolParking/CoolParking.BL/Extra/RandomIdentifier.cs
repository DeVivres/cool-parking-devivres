﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Extra
{
    public class RandomIdentifier
    {
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string RandomString(int size, bool upperCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;

            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            if (upperCase)
            {
                return builder.ToString().ToUpper();
            }
            return builder.ToString();
        }

        public string randomIdentifier(int size = 0)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(RandomString(2, true));

            builder.Append('-');

            builder.Append(RandomNumber(1000, 9999));

            builder.Append('-');

            builder.Append(RandomString(2, true));

            return builder.ToString();
        }
    }
}
